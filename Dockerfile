FROM debian
RUN apt update && \
    apt-get install -qy texlive-full \
        python-pygments gnuplot \
        make git && \
    rm -rf /var/lib/apt/lists/*

VOLUME [ "/data" ]
WORKDIR /data

CMD pdflatex template_Scrartcl.tex && bibtex template_Scrartcl && pdflatex template_Scrartcl.tex
